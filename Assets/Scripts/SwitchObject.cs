﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchObject : MonoBehaviour
{
    [SerializeField] Transform target;
    private void OnCollisionEnter(Collision collision)
    {
        if (target !=null)
        {
            Destroy(target.gameObject);
        }
    }
}
