﻿using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    private const float TIME_BEFORE_START = 3f;

    private static LevelManager instance;
    public static LevelManager Instance { get { return instance; } }
    public GameObject pauseMenu;
    public GameObject endMenu;

    private GameObject player;
    public Transform respawnPoint;

    public Text timerText;
    public Text endTimerText;
    private float levelDuration;

    private float startTime;
    public float silverTime;
    public float goldTime;
    private void Start()
    {
        endMenu.SetActive(false);
        player = GameObject.FindGameObjectWithTag("Player");
        instance = this;
        pauseMenu.SetActive(false);
        startTime = Time.time;
        player.transform.position = respawnPoint.position;
    }

    private void Update()
    {
        if(player.transform.position.y < -10)
        {
            Death();
        }
        if (Time.time - startTime < TIME_BEFORE_START)
            return;

        levelDuration = Time.time - (startTime + TIME_BEFORE_START);
        string minutes = ((int)levelDuration / 60).ToString("00");
        string seconds = (levelDuration % 60).ToString("00.00");
        timerText.text = minutes + ":" + seconds;
    }

    private void Death()
    {
        //Rigidbody rb = player.GetComponent<Rigidbody>();
        //player.transform.position = respawnPoint.position;
        //rb.velocity = Vector3.zero;
        //rb.angularVelocity = Vector3.zero;
        Restart();
    }

    public void TogglePauseMenu()
    {
        Time.timeScale = (!pauseMenu.activeSelf) ? 0 : 1;
        pauseMenu.SetActive(!pauseMenu.activeSelf);
    }
    public void Restart()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void ToMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("MainMenu");
    }
    public void Victory()
    {
        foreach (Transform t in endMenu.transform.parent)
        {
            t.gameObject.SetActive(false);
        }
        endMenu.SetActive(true);
        Rigidbody rb = player.GetComponent<Rigidbody>();
        rb.constraints = RigidbodyConstraints.FreezePosition;

        string minutes = ((int)levelDuration / 60).ToString("00");
        string seconds = (levelDuration % 60).ToString("00.00");
        endTimerText.text = minutes + ":" + seconds;
        if (levelDuration < goldTime)
        {
            GameManager.Instance.currency += 50;
            endTimerText.color = Color.yellow;
        }
        else if (levelDuration < silverTime)
        {
            GameManager.Instance.currency += 25;
            endTimerText.color = Color.gray;
        }
        else
        {
            GameManager.Instance.currency += 10;
            endTimerText.color = new Color(0.6f, .5f, .2f);
        }
        GameManager.Instance.Save();

        string saveString = "";
        //"30&60&45"
        LevelData level = new LevelData(SceneManager.GetActiveScene().name);
        saveString += (level.BestTime > levelDuration || level.BestTime == 0.0f) ? levelDuration.ToString(): level.BestTime.ToString();
        saveString += '&';
        saveString += silverTime.ToString();
        saveString += '&';
        saveString += goldTime.ToString();
        PlayerPrefs.SetString(SceneManager.GetActiveScene().name, saveString);
    }
}
