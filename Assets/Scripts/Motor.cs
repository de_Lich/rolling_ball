﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Motor : MonoBehaviour
{
    private const float TIME_BEFORE_START = 3f;
    public float moveSpeed = 5f;
    public float drag = 0.5f;
    public float terminalRotation = 25.0f;

    public VirtualJoystick moveJoystick;
    private Transform camTransform;

    public float boostSpeed = 10f;
    public float boostCooldown = 2f;
    private float lastBoost;

    private float startTime;

    private Rigidbody controller;
    private void Start()
    {
        lastBoost = Time.time - boostCooldown;
        startTime = Time.time;
        controller = GetComponent<Rigidbody>();
        controller.drag = drag;
        controller.maxAngularVelocity = terminalRotation;
        camTransform = Camera.main.transform;
    }
    void Update()
    {
        if (Time.time - startTime < TIME_BEFORE_START)
            return;

        Vector3 dir = Vector3.zero;
        dir.x = Input.GetAxis("Horizontal");
        dir.z = Input.GetAxis("Vertical");

        if (dir.magnitude > 1)
            dir.Normalize();

        if(moveJoystick.InputDirection != Vector3.zero)
        {
            dir = moveJoystick.InputDirection;
        }
        else if (Time.timeScale == 0)
        {
            dir = Vector3.zero;
        }
        Vector3 rotatedDir = camTransform.TransformDirection(dir);
        rotatedDir = new Vector3(rotatedDir.x, 0, rotatedDir.z);
        rotatedDir = rotatedDir.normalized * dir.magnitude;

        controller.AddForce(rotatedDir * moveSpeed);
    }

    public void Boost()
    {
        if (Time.time - startTime < TIME_BEFORE_START)
            return;

        if (Time.time - lastBoost > boostCooldown)
        {
            lastBoost = Time.time;
            controller.AddForce(controller.velocity.normalized * boostSpeed, ForceMode.VelocityChange);

        }
    }
}
