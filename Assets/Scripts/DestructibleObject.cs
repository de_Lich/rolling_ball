﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructibleObject : MonoBehaviour
{
    [SerializeField] float force = 10.0f;
    [SerializeField] GameObject ruinDoor;
    
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.impulse.magnitude > force)
        {
            GameObject fx = Instantiate(ruinDoor, collision.transform.position, Quaternion.identity);
            fx.transform.position = gameObject.transform.position;
            Destroy(gameObject);
            Debug.Log(collision.impulse);
        }
    }
}
