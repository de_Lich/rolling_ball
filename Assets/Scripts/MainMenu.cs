﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelData
{
    public LevelData(string leveName)
    {
        string data = PlayerPrefs.GetString(leveName);
        if (data == "")
            return;
        string[] allData = data.Split('&');
        BestTime = float.Parse(allData[0]);
        SilverTime = float.Parse(allData[1]);
        GoldTime = float.Parse(allData[2]);
    }
    public float BestTime { set; get; }
    public float SilverTime { set; get; }
    public float GoldTime { set; get; }
}
public class MainMenu : MonoBehaviour
{
    private const float CAMERA_ROTATION_SPEED = 3f;

    public Sprite[] stars;
    public GameObject levelButtonPrefab;
    public GameObject levelButtonContainer;
    public GameObject shopItemButton;
    public GameObject shopItemContainer;
    public Text currencyText;

    public Material playerMaterial;

    private Transform cameraTransform;
    private Transform cameraDesiredLookAt;

    private bool nextLevelLocked = false;

    private int[] costs = { 0, 50, 50, 50, 150, 200, 200, 200, 500, 500, 500, 500, 1000, 1250, 1500, 1750 };

    private void Start()
    {
        ChangePlayerSkin(GameManager.Instance.currentSkinIndex);
        currencyText.text = "Currency : " + GameManager.Instance.currency.ToString();
        cameraTransform = Camera.main.transform;
        Sprite[] thumbnails = Resources.LoadAll<Sprite>("Levels");
        foreach (var thumbnail in thumbnails)
        {
            GameObject container = Instantiate(levelButtonPrefab) as GameObject;
            container.GetComponent<Image>().sprite = thumbnail;
            container.transform.SetParent(levelButtonContainer.transform, false);
            LevelData level = new LevelData(thumbnail.name);

            string minutes = ((int)level.BestTime / 60).ToString("00");
            string seconds = (level.BestTime % 60).ToString("00.00");

            GameObject starsImage = container.transform.GetChild(0).GetChild(1).gameObject;
            container.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = (level.BestTime != 0.0f) ? minutes + ":" + seconds : "Not complete";

            container.transform.GetChild(1).GetComponent<Image>().enabled = nextLevelLocked;
            container.GetComponent<Button>().interactable = !nextLevelLocked;

            if(level.BestTime == 0.0f)
            {
                starsImage.SetActive(false);
                nextLevelLocked = true;
            }
            else if(level.BestTime < level.GoldTime)
            {
                starsImage.GetComponentInParent<Image>().sprite = stars[2];
            }
            else if(level.BestTime < level.SilverTime)
            {
                starsImage.GetComponentInParent<Image>().sprite = stars[1];
            }
            else
            {
                starsImage.GetComponentInParent<Image>().sprite = stars[0];
            }

            string sceneName = thumbnail.name;
            container.GetComponent<Button>().onClick.AddListener(()=> LoadLevel(sceneName));
        }
        Sprite[] items = Resources.LoadAll<Sprite>("Player");
        int indexTexture = 0;
        foreach (var item in items)
        {
            GameObject container = Instantiate(shopItemButton) as GameObject;
            container.GetComponent<Image>().sprite = item;
            container.transform.SetParent(shopItemContainer.transform, false);
            int index = indexTexture;
            container.GetComponent<Button>().onClick.AddListener(() => ChangePlayerSkin(index));
            container.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = costs [index].ToString();
                //container.transform.GetComponentInChildren<Text>()
            if((GameManager.Instance.skinAvailibility & 1 << index) == 1 << index)
            {
                container.transform.GetChild(0).gameObject.SetActive(false);
            }
            indexTexture++; 
        }
    }
    private void Update()
    {
        if(cameraDesiredLookAt != null)
        {
            cameraTransform.rotation = Quaternion.Slerp(cameraTransform.rotation, cameraDesiredLookAt.rotation, CAMERA_ROTATION_SPEED * Time.deltaTime);
        }
    }
    private void ChangePlayerSkin(int index)
    {
        if ((GameManager.Instance.skinAvailibility & 1 << index) == 1 << index)
        {
            float x = (index % 4) * 0.25f;
            float y = ((int)index / 4) * 0.25f;

            if (y == 0f)
                y = 0.75f;
            else if (y == 0.25f)
                y = .5f;
            else if (y == .5f)
                y = .25f;
            else if (y == 0.75f)
                y = 0f;

            playerMaterial.SetTextureOffset("_MainTex", new Vector2(x, y));
            GameManager.Instance.currentSkinIndex = index;
            GameManager.Instance.Save();
        }
        else
        {
            int cost = costs[index];
            if(GameManager.Instance.currency >= cost)
            {
                GameManager.Instance.currency -= cost;
                GameManager.Instance.skinAvailibility += 1 << index;
                GameManager.Instance.Save();
                currencyText.text = "Currency : " + GameManager.Instance.currency.ToString();                   
                shopItemContainer.transform.GetChild(index).GetChild(0).gameObject.SetActive(false);
                ChangePlayerSkin(index);
            }
        }
    }
    private void LoadLevel(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }
    public void LookAtMenu(Transform menuTransform)
    {
        cameraDesiredLookAt = menuTransform;
    }
}
